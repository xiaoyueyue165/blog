blog
====

my personal blog


## 深入系列

#### - [一张网页，要经历怎样的过程，才能抵达用户面前？](https://github.com/xiaoyueyue165/blog/blob/master/docs/%E4%B8%80%E5%BC%A0%E7%BD%91%E9%A1%B5%EF%BC%8C%E8%A6%81%E7%BB%8F%E5%8E%86%E6%80%8E%E6%A0%B7%E7%9A%84%E8%BF%87%E7%A8%8B%EF%BC%8C%E6%89%8D%E8%83%BD%E6%8A%B5%E8%BE%BE%E7%94%A8%E6%88%B7%E9%9D%A2%E5%89%8D%EF%BC%9F.md) 
#### - [前端模拟 API 数据的两种方式](https://github.com/xiaoyueyue165/blog/issues/25)

## React

#### - [React组件通信 ](https://github.com/xiaoyueyue165/blog/issues/27)
#### - [redux入门](https://github.com/xiaoyueyue165/blog/issues/34)

## Javascript

#### - [Javascript编程风格](https://github.com/xiaoyueyue165/blog/issues/11)
#### - [Javascript继承模式](https://github.com/xiaoyueyue165/blog/issues/17)
#### - [Javascript模块化](https://github.com/xiaoyueyue165/blog/issues/23)
#### - [Javascript设计模式](https://github.com/xiaoyueyue165/blog/issues/26)
#### - [函数声明与函数表达式的区别](https://github.com/xiaoyueyue165/blog/issues/10)

## 思考

#### - [No game 赌约](https://github.com/xiaoyueyue165/blog/issues/13)

## 工程化，规范化

#### - [webpack使用说明](https://github.com/xiaoyueyue165/blog/issues/33)
#### - [使用fis3构建工程化项目](https://github.com/xiaoyueyue165/blog/issues/14) 
#### - [babel使用全纪录](https://github.com/xiaoyueyue165/blog/issues/16) 

## 开发环境

#### - [npm&yarn](https://github.com/xiaoyueyue165/blog/issues/7)
#### - [开发环境的搭建](https://github.com/xiaoyueyue165/blog/issues/3)
#### - [git+Github的正确姿势 ](https://github.com/xiaoyueyue165/blog/issues/2)
#### - [git 入门与实践 ](https://github.com/xiaoyueyue165/blog/issues/1)

## Interview

#### - [front-end-interview-handbook JS问题](https://github.com/xiaoyueyue165/blog/issues/15)

## 优秀的文章

- [如何在疲劳的JS世界中持续学习](https://zhuanlan.zhihu.com/p/36339128)
- [驳《我不是很懂 Node.js 社区的 DRY 文化》](https://segmentfault.com/a/1190000014480379) by [justjavac](https://segmentfault.com/u/justjavac) 【2018.4.20】
- [一个程序员的成长之路](https://github.com/fouber/blog/issues/41) by [fouber](https://github.com/fouber)
- [页面可视化搭建工具前生今世](https://zhuanlan.zhihu.com/p/37171897)

## Useful links

- [segmentfault](https://segmentfault.com/u/xiaoyueyue165)
- [stackoverflow](https://stackoverflow.com/users/8273471/xiaoyueyue)
- [zhihu](https://www.zhihu.com/people/yan-yue-44-30/activities)
- [infoq](http://www.infoq.com/cn/)
- [GitHub 全球 Developers, Organizations and Repositories 排行榜](https://www.diycode.cc/trends)
- [location:China](https://github.com/search?q=location%3AChina)



